## Usage Help

```
# Step 1
Downlaod or clone vee-validate.js file to nuxt.js project plugins folder

# Step 2
Add plugin to nuxt.config.js:

export default {
    plugins: [
        { src: '@/plugins/vee-validate' },
    ]
}

# Done!

Run your project and enjoy it.

```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
