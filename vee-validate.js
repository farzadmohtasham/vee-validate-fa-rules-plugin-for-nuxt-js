// eslint-disable-next-line import/no-extraneous-dependencies
import Vue from 'vue';
import { extend, ValidationObserver, ValidationProvider } from 'vee-validate';
import {
  alpha,
  alpha_num,
  length,
  max,
  min,
  numeric,
  required,
  size,
  email,
} from 'vee-validate/dist/rules';

extend('required', {
  ...required,
  message: 'این فیلد نمی‌تواند خالی بماند',
});

extend('email', {
  ...email,
  message: 'لطفا ایمیل معتبری را وارد کنید',
});

extend('min', {
  ...min,
  message: 'طول این فیلد نمی‌تواند کمتر از {length} کاراکتر باشد',
});

extend('max', {
  ...max,
  message: 'طول این فیلد نمی‌تواند بیشتر از {length} کاراکتر باشد',
});

extend('numeric', {
  ...numeric,
  message: 'این فیلد بایستی فقط شامل اعداد انگلیسی باشد',
});

extend('length', {
  ...length,
  message: 'تعداد کاراکتر‌های این فیلد بایستی برابر با {length} باشد',
});

extend('alpha', {
  ...alpha,
  message: 'این فیلد فقط می‌تواند شامل حروف انگلیسی باشد',
});

extend('alpha_num', {
  ...alpha_num,
  message: 'این فیلد فقط می‌تواند شامل حرف و اعداد انگلیسی باشد',
});

extend('size', {
  ...size,
  message: 'حجم فایل نمی‌تواند بیشتر از {size} کیلوبایت باشد',
});

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
